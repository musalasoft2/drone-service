package com.musala.soft.controller;

import java.util.List;

import com.musala.soft.domain.DroneModel;
import com.musala.soft.service.DroneModelService;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import jakarta.inject.Inject;

@Controller("/model")
public class DroneModelController {
    
    @Inject
    private DroneModelService droneModelService;

    @Get("/")
    public HttpResponse<List<DroneModel>> list() {
        return HttpResponse.ok( this.droneModelService.findAll() );
    }
}
