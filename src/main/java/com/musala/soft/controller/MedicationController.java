package com.musala.soft.controller;

import java.util.List;

import com.musala.soft.domain.Medication;
import com.musala.soft.service.MedicationService;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import jakarta.inject.Inject;

@Controller("/medication")
public class MedicationController {
    
    @Inject
    private MedicationService medicationService;

    @Get("/")
    public HttpResponse<List<Medication>> list() {
        return HttpResponse.ok( this.medicationService.findAll() );
    }
}
