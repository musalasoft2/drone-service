package com.musala.soft.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.musala.soft.util.exceptions.DomainObjectNotFoundException;
import com.musala.soft.util.exceptions.ServerException;

import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Error;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.hateoas.JsonError;
import io.micronaut.http.hateoas.Link;

@Controller("/application")
public class ApplicationController {

    private static final Logger LOG = LoggerFactory.getLogger(ApplicationController.class);
    

    @Get("/")
    HttpResponse<String> greetings() {
        return HttpResponse.ok("Server is running!");
    }

    @Error(global = true)
    HttpResponse<JsonError> entityNotFoundExceptionHandler(HttpRequest<?> request, DomainObjectNotFoundException ex) {
        JsonError error = new JsonError("Error: " + ex.getMessage()).link(Link.SELF, Link.of(request.getUri()));
        LOG.error(ex.getMessage(), ex);
        return HttpResponse.notFound(error);
    }

    @Error(global = true)
    HttpResponse<JsonError> entityNotFoundExceptionHandler(HttpRequest<?> request, ServerException ex) {
        JsonError error = new JsonError("Error: " + ex.getMessage()).link(Link.SELF, Link.of(request.getUri()));
        LOG.error(ex.getMessage(), ex);
        return HttpResponse.badRequest(error);
    }

}
