package com.musala.soft.controller;

import com.musala.soft.util.dto.in.DroneCreationDto;
import com.musala.soft.util.dto.in.DroneLoadDto;
import com.musala.soft.util.dto.out.BatteryLevelDto;

import java.util.List;

import com.musala.soft.domain.Drone;
import com.musala.soft.domain.DroneState;
import com.musala.soft.domain.MedicationLoad;
import com.musala.soft.service.DroneService;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import jakarta.inject.Inject;

@Controller("/dispatch")
public class DispatchController {
    
    @Inject
    private DroneService droneService;

    @Get("/")
    public HttpResponse<List<Drone>> list() {
        return HttpResponse.ok( this.droneService.findAll() );
    }
    
    @Get("/{id}")
    public HttpResponse<Drone> get(@PathVariable("id") Long id) {
        return HttpResponse.ok( this.droneService.getDroneById(id) );
    }
    
    @Post("/")
    public HttpResponse<Drone> createDrone(@Body DroneCreationDto dto) {
        return HttpResponse.ok( this.droneService.createDrone(dto) );
    }

    @Put("/{id}/state/{state}")
    public HttpResponse<Drone> changeDroneState(@PathVariable("id") Long id, @PathVariable("state") DroneState state) {
        return HttpResponse.ok( this.droneService.changeDroneState(id, state));
    }

    @Post("/{id}/load")
    public HttpResponse<List<MedicationLoad>> loadDrone(@PathVariable("id") Long id, @Body DroneLoadDto dto) {
        return HttpResponse.ok( this.droneService.loadDrone(id, dto) );
    }

    @Get("/{id}/load")
    public HttpResponse<List<MedicationLoad>> getDroneLoad(@PathVariable("id") Long id) {
        return HttpResponse.ok( this.droneService.getDroneLoad(id) );
    }

    @Get("/state/{state}")
    public HttpResponse<List<Drone>> getDronesByState(@PathVariable("state") DroneState state) {
        return HttpResponse.ok( this.droneService.getDronesByState(state) );
    }

    @Get("/{id}/battery")
    public HttpResponse<BatteryLevelDto> getDroneBatteryLevel(@PathVariable("id") Long id) {
        return HttpResponse.ok( this.droneService.getDroneBatteryLevel(id) );
    }
}
