package com.musala.soft.repository;

import java.util.Optional;

import com.musala.soft.domain.DroneState;
import com.musala.soft.domain.DroneStateTransition;

import io.micronaut.data.annotation.Query;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.CrudRepository;

@Repository
public interface DroneStateTransitionRepository extends CrudRepository<DroneStateTransition, Long> {
    
    public Optional<DroneStateTransition> findByOriginStateAndValidEndStates(DroneState originState, DroneState endState);

    @Query("SELECT count(s) FROM DroneStateTransition s INNER JOIN s.validEndStates ve WHERE s.originState = :originState AND ve = :endState")
    public Long countByOriginStateAndValidEndStates(DroneState originState, DroneState endState);

}
