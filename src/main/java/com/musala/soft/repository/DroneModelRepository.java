package com.musala.soft.repository;

import java.util.List;

import com.musala.soft.domain.DroneModel;

import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.PageableRepository;

@Repository
public interface DroneModelRepository extends PageableRepository<DroneModel, Long> {
    
    public List<DroneModel> findAll();
}
