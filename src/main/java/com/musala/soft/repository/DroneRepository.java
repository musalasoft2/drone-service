package com.musala.soft.repository;

import java.util.List;
import java.util.Optional;

import com.musala.soft.domain.Drone;
import com.musala.soft.domain.DroneState;

import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.PageableRepository;

@Repository
public interface DroneRepository extends PageableRepository<Drone, Long> {

    List<Drone> findAll();

    List<Drone> findByState(DroneState state);

    Optional<Drone> findById(Long id);
}
