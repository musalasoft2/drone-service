package com.musala.soft.repository;

import java.util.List;
import java.util.Optional;

import com.musala.soft.domain.Medication;

import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.CrudRepository;

@Repository
public interface MedicationRepository extends CrudRepository<Medication, Long> {
    
    List<Medication> findAll();
    Optional<Medication> findById( Long id );
}
