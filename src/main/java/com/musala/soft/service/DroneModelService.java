package com.musala.soft.service;

import java.util.List;

import com.musala.soft.domain.DroneModel;
import com.musala.soft.repository.DroneModelRepository;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;

@Singleton
public class DroneModelService {
    
    @Inject
    private DroneModelRepository modelRepository;

    public List<DroneModel> findAll() {
        return this.modelRepository.findAll();
    }
}
