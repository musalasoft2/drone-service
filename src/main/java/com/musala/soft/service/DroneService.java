package com.musala.soft.service;

import java.util.List;
import java.util.Optional;

import com.musala.soft.domain.Drone;
import com.musala.soft.domain.DroneModel;
import com.musala.soft.domain.DroneState;
import com.musala.soft.domain.Medication;
import com.musala.soft.domain.MedicationLoad;
import com.musala.soft.repository.DroneRepository;
import com.musala.soft.repository.DroneModelRepository;
import com.musala.soft.util.dto.in.DroneCreationDto;
import com.musala.soft.util.dto.in.DroneLoadDto;
import com.musala.soft.util.dto.out.BatteryLevelDto;
import com.musala.soft.util.exceptions.DroneLoadExceededException;
import com.musala.soft.util.exceptions.IncorrectDroneStateException;
import com.musala.soft.util.exceptions.DomainObjectNotFoundException;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;

@Singleton
public class DroneService {
    
    @Inject
    private DroneRepository droneRepository;

    @Inject
    private DroneModelRepository modelRepository;

    @Inject
    private DroneStateTransitionService droneStateTransitionService;

    @Inject
    private MedicationService medicationService;

    public Drone createDrone(DroneCreationDto dto) {
        Optional<DroneModel> optionalMode = this.modelRepository.findById(dto.getDroneModelId());
        if (!optionalMode.isPresent())
            throw new DomainObjectNotFoundException(DroneModel.class, dto.getDroneModelId());
        
        DroneModel model = optionalMode.get();
        Drone newDrone = new Drone(dto.getSerialNumber(), dto.getWeightLimit(), model, dto.getBatteryCapacity());
        return this.droneRepository.save(newDrone);
    }
    
    public Drone changeDroneState(Long id, DroneState endState) {
        Drone drone = this.getDroneById(id);
        
        this.droneStateTransitionService.validateStateTransition(drone, endState);

        drone.setState(endState);
        return this.droneRepository.update(drone);
    }

    public List<MedicationLoad> loadDrone(Long id, DroneLoadDto dto) {
        Drone drone = this.getDroneById(id);
        if (drone.getState() != DroneState.LOADING)
            throw new IncorrectDroneStateException(drone, "load");
            
        Medication meds = this.medicationService.getMedicationById(dto.getMedicationId());
        double loadWeight = meds.getWeight() * dto.getQuantity();
        if (drone.getRemainingCapacity() < loadWeight )
            throw new DroneLoadExceededException(drone.getWeightLimit());

        drone.loadMedication(meds, dto.getQuantity());
        return this.droneRepository.update(drone).getLoad();
    }


    public List<MedicationLoad> getDroneLoad(Long id) {
        Drone drone = this.getDroneById(id);
        return drone.getLoad();
    }

    public List<Drone> getDronesByState(DroneState state) {
        return this.droneRepository.findByState(state);
    }

    public BatteryLevelDto getDroneBatteryLevel(Long id) {
        Drone drone = this.getDroneById(id);
        return new BatteryLevelDto( drone.getBatteryCapacity() );
    }

    public List<Drone> findAll() {
        return this.droneRepository.findAll();
    }

    public Drone getDroneById(Long id) {
        Optional<Drone> optionalDrone = this.droneRepository.findById(id);
        if (!optionalDrone.isPresent())
            throw new DomainObjectNotFoundException(Drone.class, id);

        return optionalDrone.get();
    }

}
