package com.musala.soft.service;

import java.util.HashMap;
import java.util.Map;

import com.musala.soft.domain.Drone;
import com.musala.soft.domain.DroneState;
import com.musala.soft.repository.DroneStateTransitionRepository;
import com.musala.soft.util.exceptions.InvalidDroneStateTransitionException;
import com.musala.soft.util.validators.LoadingStateRequirementValidator;
import com.musala.soft.util.validators.StateRequirementValidator;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;

@Singleton
public class DroneStateTransitionService {

    /*
     * Custom validators for each state that requires any
     */
    private final Map<DroneState, StateRequirementValidator> validators = new HashMap<DroneState, StateRequirementValidator>() {{
        put(DroneState.LOADING, new LoadingStateRequirementValidator());
    }};
    
    @Inject
    private DroneStateTransitionRepository droneStateTransitionRepository;

    public void validateStateTransition(Drone drone, DroneState endState) {
        DroneState originState = drone.getState();
        this.validateOriginStateToEndStateTransition(originState, endState);
        this.validateDroneMeetsEndStateRequirements(drone, endState);
        
    }

    private void validateOriginStateToEndStateTransition(DroneState originState, DroneState endState) {
        if (this.droneStateTransitionRepository.countByOriginStateAndValidEndStates(originState, endState) == 0L)
            throw new InvalidDroneStateTransitionException(originState, endState);
    }

    private void validateDroneMeetsEndStateRequirements(Drone drone, DroneState endState) {
        StateRequirementValidator validator = this.validators.get(endState);
        if (validator != null)
            validator.validate(drone);
    }
    
}
