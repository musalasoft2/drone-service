package com.musala.soft.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.musala.soft.domain.Drone;
import com.musala.soft.util.file.CsvConfiguration;
import com.musala.soft.util.file.FileServiceUtil;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;

@Singleton
public class DroneBatteryService {
    
    private static final Logger LOG = LoggerFactory.getLogger(DroneBatteryService.class);

    @Inject
    private DroneService droneService;

    /*
     * Assumption: A drone's serial code is unique and will never change
     * Otherwise a more flexible solution could be to record battery levels on the database, using a drone's
     * id for reference. 
     */
    public void checkAndLogDroneBatteryLevels() {
        List<Drone> drones = this.droneService.findAll();
        List<String> headers = Arrays.asList("Time", "Serial code", "Battery");
        List<List<String>> newRows = new ArrayList<List<String>>();
        drones.forEach(d -> {
            newRows.add( Arrays.asList(LocalDateTime.now().toString(), d.getSerialNumber().toString(), d.getBatteryCapacity().toString()) );
        });

        CsvConfiguration config = new CsvConfiguration(headers, this.getFileName(), newRows);
        
        try {
            FileServiceUtil.buildCsvFile(config);
        } catch (IOException e) {
            LOG.error("Error on writting drone battery levels on CSV file " + e.getMessage());
        }
    }

    private String getFileName() {
        return LocalDate.now() + " Drone Battery Logs";
    }
}
