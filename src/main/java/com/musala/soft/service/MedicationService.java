package com.musala.soft.service;

import java.util.List;
import java.util.Optional;

import com.musala.soft.domain.Medication;
import com.musala.soft.repository.MedicationRepository;
import com.musala.soft.util.exceptions.DomainObjectNotFoundException;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;

@Singleton
public class MedicationService {
    
    @Inject
    private MedicationRepository medicationRepository;

    public List<Medication> findAll() {
        return this.medicationRepository.findAll();
    }

    public Medication getMedicationById(Long id) {
        Optional<Medication> optionalMeds = this.medicationRepository.findById(id);
        if (!optionalMeds.isPresent())
            throw new DomainObjectNotFoundException(Medication.class, id);

        return optionalMeds.get();
    }
}
