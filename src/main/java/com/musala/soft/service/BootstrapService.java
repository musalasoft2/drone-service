package com.musala.soft.service;

import java.util.Arrays;
import java.util.List;

import javax.transaction.Transactional;

import com.musala.soft.domain.Drone;
import com.musala.soft.domain.DroneModel;
import com.musala.soft.domain.DroneState;
import com.musala.soft.domain.DroneStateTransition;
import com.musala.soft.domain.Medication;
import com.musala.soft.repository.DroneRepository;
import com.musala.soft.repository.DroneStateTransitionRepository;
import com.musala.soft.repository.MedicationRepository;
import com.musala.soft.repository.DroneModelRepository;

import io.micronaut.context.event.StartupEvent;
import io.micronaut.runtime.event.annotation.EventListener;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;

@Singleton
public class BootstrapService {

    @Inject
    DroneModelRepository modelRepository;

    @Inject
    DroneRepository droneRepository;

    @Inject
    DroneStateTransitionRepository droneStateTransitionRepository;

    @Inject
    MedicationRepository medicationRepository;

    @EventListener
    @Transactional
    void initializeDatabase(StartupEvent event) {
        this.initializeDroneModels();
        this.initializeDrones();
        this.initializeDroneStateTransitions();
        this.initializeMedicationSample();
    }
    
    private void initializeDroneModels() {
        if (this.modelRepository.count() != 0)
            return;

        List<DroneModel> models = Arrays.asList(
            new DroneModel("Lightweight"),
            new DroneModel("Middleweight"),
            new DroneModel("Cruiserweight"),
            new DroneModel("Heavyweight")
        );
        models.forEach(x -> this.modelRepository.save(x));
    }

    private void initializeDrones() {
        if (this.droneRepository.count() != 0)
            return;

        List<DroneModel> models = this.modelRepository.findAll();

        List<Drone> drones = Arrays.asList(
            new Drone("AS1003220212000001", 150, models.get(0), 100),
            new Drone("AS1003220212000002", 150, models.get(0), 100),
            new Drone("AS1003220212000003", 200, models.get(1), 100),
            new Drone("AS1003220212000004", 200, models.get(1), 100),
            new Drone("AS1003220212000005", 250, models.get(1), 75),
            new Drone("AS1003220212000006", 300, models.get(2), 75),
            new Drone("AS1003220212000007", 350, models.get(2), 60),
            new Drone("AS1003220212000008", 400, models.get(3), 50),
            new Drone("AS1003220212000009", 450, models.get(3), 25),
            new Drone("AS1003220212000010", 500, models.get(3), 10)
        );
        drones.forEach(x -> this.droneRepository.save(x));
    }

    private void initializeDroneStateTransitions() {
        if (this.droneStateTransitionRepository.count() != 0)
            return;

        List<DroneStateTransition> transitions = Arrays.asList(
            new DroneStateTransition(DroneState.IDLE, Arrays.asList(DroneState.LOADING)),
            new DroneStateTransition(DroneState.LOADING, Arrays.asList(DroneState.LOADED, DroneState.IDLE)),
            new DroneStateTransition(DroneState.LOADED, Arrays.asList(DroneState.DELIVERING, DroneState.LOADING)),
            new DroneStateTransition(DroneState.DELIVERING, Arrays.asList(DroneState.DELIVERED)),
            new DroneStateTransition(DroneState.DELIVERED, Arrays.asList(DroneState.RETURNING)),
            new DroneStateTransition(DroneState.RETURNING, Arrays.asList(DroneState.IDLE))
        );
        transitions.forEach(x -> this.droneStateTransitionRepository.save(x));
    }

    private void initializeMedicationSample() {
        if (this.medicationRepository.count() != 0)
            return;

        List<Medication> meds = Arrays.asList(
            new Medication("Med_1", 15, "M001"),
            new Medication("Med_2", 5, "M002"),
            new Medication("Med_3", 7.5, "M003"),
            new Medication("Med_4", 10, "M004"),
            new Medication("Med_5", 12, "M005"),
            new Medication("Med_6", 25, "M006"),
            new Medication("Med_7", 17, "M007")
        );

        meds.forEach(x -> this.medicationRepository.save(x));
    }
}
