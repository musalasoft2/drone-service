package com.musala.soft.util.validators;

import com.musala.soft.domain.Drone;
import com.musala.soft.util.exceptions.LoadingStateRequirementsNotMetException;

public class LoadingStateRequirementValidator implements StateRequirementValidator {
    
    public void validate(Drone drone) {
        if ( drone.getBatteryCapacity() < 25) 
            throw new LoadingStateRequirementsNotMetException(drone);
    }

}
