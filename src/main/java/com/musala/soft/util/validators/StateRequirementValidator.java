package com.musala.soft.util.validators;

import com.musala.soft.domain.Drone;

public interface StateRequirementValidator {

    public void validate(Drone drone);
        
}
