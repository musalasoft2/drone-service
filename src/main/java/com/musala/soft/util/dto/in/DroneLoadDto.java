package com.musala.soft.util.dto.in;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DroneLoadDto {
    private Long medicationId;
    private Integer quantity;
}
