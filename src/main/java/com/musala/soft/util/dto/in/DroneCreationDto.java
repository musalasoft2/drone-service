package com.musala.soft.util.dto.in;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DroneCreationDto {
    private String serialNumber;
    private Integer weightLimit;
    private Long droneModelId;
    private Integer batteryCapacity;
}
