package com.musala.soft.util.dto.out;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class BatteryLevelDto {
    private Integer batteryLevel;
}
