package com.musala.soft.util.file;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class CsvConfiguration {
    String filenamePrefix;
    final String filenameSufix = ".csv";
    List<String> headers;
    List<List<String>> rows;

    public CsvConfiguration(List<String> headers, String filenamePrefix, List<List<String>> rows) {
        this.headers = headers;
        this.filenamePrefix = filenamePrefix;
        this.rows = rows;
    }

    public String getFilename() {
        return this.filenamePrefix + this.filenameSufix;
    }
}
