package com.musala.soft.util.file;


import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.List;

import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.io.FileUtils;

public class FileServiceUtil {

    private static final String filesPath = "/app/files/";
    private static final CSVFormat csvFormat = CSVFormat.Builder.create()
    .setIgnoreSurroundingSpaces(true)
    .setNullString("")
    .setDelimiter(",")
    .build();

    public static void buildCsvFile(CsvConfiguration configuration) throws IOException {
        final File file = new File(filesPath + configuration.getFilename());
        boolean newFileCreated = file.createNewFile();

        final Writer writer = new OutputStreamWriter(FileUtils.openOutputStream(file, true), "UTF-8");
        final CSVPrinter printer = csvFormat.print(writer);
        if (newFileCreated && configuration.headers != null)
            printer.printRecord(configuration.headers);

        for (List<String> row: configuration.rows) {
            printer.printRecord(row);
        }

        writer.close();
        printer.close();
    }
}
