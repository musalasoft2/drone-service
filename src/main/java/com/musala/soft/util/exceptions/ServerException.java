package com.musala.soft.util.exceptions;

public class ServerException extends RuntimeException {
    protected ServerException(String msg) {
        super(msg);
    }
}
