package com.musala.soft.util.exceptions;

public abstract class StateRequirementsNotMetException extends ServerException {
    protected StateRequirementsNotMetException(String message) {
        super(message);
    }
}
