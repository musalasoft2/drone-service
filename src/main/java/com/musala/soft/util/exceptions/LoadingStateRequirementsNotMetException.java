package com.musala.soft.util.exceptions;

import com.musala.soft.domain.Drone;

public class LoadingStateRequirementsNotMetException extends StateRequirementsNotMetException {
    public LoadingStateRequirementsNotMetException(Drone drone) {
        super("Cannot set drone " + drone.getSerialNumber() + " with battery level of " + drone.getBatteryCapacity() + "% to LOADING state");
    }
}