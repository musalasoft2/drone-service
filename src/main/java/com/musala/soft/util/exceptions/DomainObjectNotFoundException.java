package com.musala.soft.util.exceptions;

public class DomainObjectNotFoundException extends RuntimeException {
    public DomainObjectNotFoundException(Class objectClass, Long id) {
        super(objectClass.getSimpleName() + " with id " + id + " was not found.");
    }
}
