package com.musala.soft.util.exceptions;

import com.musala.soft.domain.Drone;

public class IncorrectDroneStateException extends ServerException {
    public IncorrectDroneStateException(Drone drone, String operationName) {
        super("The operation " + operationName + " cannot be executed on drone " + drone.getSerialNumber() + " with state " + drone.getState());
    }

}
