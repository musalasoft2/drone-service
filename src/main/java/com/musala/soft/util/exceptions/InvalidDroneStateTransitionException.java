package com.musala.soft.util.exceptions;

import com.musala.soft.domain.DroneState;

public class InvalidDroneStateTransitionException extends ServerException {
    
    public InvalidDroneStateTransitionException(DroneState originalState, DroneState endState) {
        super("A drone with state " + originalState + " cannot transition into state " + endState);
    }
}
