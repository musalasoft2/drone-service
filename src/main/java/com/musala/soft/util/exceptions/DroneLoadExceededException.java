package com.musala.soft.util.exceptions;

public class DroneLoadExceededException extends ServerException {
    public DroneLoadExceededException(Integer wieghtLimit) {
        super("Weight limit of " + wieghtLimit + "g exceeded");
    }
}
