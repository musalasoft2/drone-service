package com.musala.soft.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class DroneStateTransition {
    
    @Id @GeneratedValue
    private Long id;

    @Column(unique = true)
    @Enumerated(EnumType.STRING)
    private DroneState originState;

    @Enumerated(EnumType.STRING)
    @ElementCollection(targetClass = DroneState.class)
    private List<DroneState> validEndStates;

    public DroneStateTransition(DroneState originState, List<DroneState> validEndStates) {
        this.originState = originState;
        this.validEndStates = validEndStates;
    }
}
