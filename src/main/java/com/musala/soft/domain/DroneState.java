package com.musala.soft.domain;

public enum DroneState {
    IDLE, 
    LOADING, 
    LOADED, 
    DELIVERING, 
    DELIVERED, 
    RETURNING
}
