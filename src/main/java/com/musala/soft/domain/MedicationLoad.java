package com.musala.soft.domain;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class MedicationLoad {

    public MedicationLoad(Medication medication, Integer quantity) {
        this.medication = medication;
        this.quantity = quantity;
        this.loadTime = LocalDateTime.now();
    }
    
    @Id @GeneratedValue
    private Long id;

    private LocalDateTime loadTime;
    
    @OneToOne
    private Medication medication;

    private Integer quantity;

    public double getWeight() {
        return this.medication.getWeight() * this.quantity;
    }
    
}
