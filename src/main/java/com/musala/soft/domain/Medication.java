package com.musala.soft.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.validation.constraints.Pattern;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class Medication {
    
    @Id @GeneratedValue
    private Long id;

    @Pattern(regexp = "^[A-Za-z0-9_-]+$")
    private String name;
    
    /**
     * I'm using double given the simplcity of the scope.
     * If I was working with numbers that required higher precision, like with money, I would use BigDecimal instead
     * */
    private double weight;
    
    @Pattern(regexp = "^[A-Z0-9_]+$")
    private String code;

    @Lob
    private byte[] image;

    public Medication(String name, double weight, String code) {
        this.name = name;
        this.weight = weight;
        this.code = code;
    }
    
}
