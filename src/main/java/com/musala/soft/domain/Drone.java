package com.musala.soft.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.Max;

import lombok.Data;
import lombok.NoArgsConstructor;


@Entity 
@Data 
@NoArgsConstructor
public class Drone {
    
    @Id
    @GeneratedValue
    private Long id;

    @Column(length = 100)
    private String serialNumber;

    @ManyToOne
    private DroneModel model;

    @Max(value = 500L) 
    private Integer weightLimit;

    @Max(value = 100L)
    private Integer batteryCapacity;

    @Enumerated(EnumType.STRING)
    private DroneState state;

    /**  
     * For a fully working application, I would use a separate class containing
     * the list of medication loads, as well as the departure date and date of arrival of the drone.
     * I decided to keep it simple given the scope of this task.
    */
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<MedicationLoad> load;

    public Drone(String serialNumber, Integer weightLimit, DroneModel model, Integer batteryCapacity) {
        this.serialNumber = serialNumber;
        this.weightLimit = weightLimit;
        this.model = model;
        this.state = DroneState.IDLE;
        this.batteryCapacity = (batteryCapacity != null) ? batteryCapacity : 100;
    }

    public double getRemainingCapacity() {
        double currentLoad = (this.load == null || this.load.size() ==0 ) ? Double.valueOf(0) :
            this.load.stream()
            .map(x -> x.getWeight())
            .reduce(Double.valueOf(0), (subtotal, newElement) -> subtotal + newElement);

        return this.weightLimit - currentLoad;
    }

    public void loadMedication(Medication medication, Integer quantity) {
        if (this.load == null)
            this.load = new ArrayList<MedicationLoad>();
        this.load.add(new MedicationLoad(medication, quantity));
    }
}
