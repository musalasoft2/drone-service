package com.musala.soft.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
/**
 * I'm using a common class instead of an enum for the flexibility of adding new drone models to the database on demand,
 * whereas if I were to use an enum and was asked to add a new model, I would need to compile a new version of the app
 * with the extra enum value and deploy it.
 */
public class DroneModel {
    
    public DroneModel(String name) {
        this.name = name;
    }

    @Id @GeneratedValue
    private Long id;

    private String name;
}
