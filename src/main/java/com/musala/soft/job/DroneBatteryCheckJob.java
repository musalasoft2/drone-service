package com.musala.soft.job;

import com.musala.soft.service.DroneBatteryService;

import io.micronaut.scheduling.annotation.Scheduled;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;

@Singleton
public class DroneBatteryCheckJob {

    @Inject
    private DroneBatteryService droneBatteryService;
    
    /*
     * Logs battery level for every drone in the database every hour
     */
    @Scheduled(cron = "0 * * * *") 
    void execute() {
        this.droneBatteryService.checkAndLogDroneBatteryLevels();
    }
        
}
