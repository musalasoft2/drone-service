package com.musala.soft.service;

import com.musala.soft.domain.Drone;
import com.musala.soft.domain.DroneState;
import com.musala.soft.util.exceptions.LoadingStateRequirementsNotMetException;
import com.musala.soft.util.validators.LoadingStateRequirementValidator;

import io.micronaut.test.annotation.MockBean;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

@MicronautTest
public class DroneStateTransitionServiceTest {
    
    @Inject
    private DroneStateTransitionService droneStateTransitionService;

    @Test
    void testStateTransitionAllowed() {
        DroneState originState = DroneState.IDLE;
        Drone mockDrone = new Drone();
        mockDrone.setBatteryCapacity(100);
        mockDrone.setState(originState);
        DroneState endState = DroneState.LOADING;
            
        assertAll(() -> droneStateTransitionService.validateStateTransition(mockDrone, endState));
    }

    @Test
    void testLowBatteryThrowsRequirementsNotMetException() {
        DroneState originState = DroneState.IDLE;
        Drone mockDrone = new Drone();
        mockDrone.setBatteryCapacity(10);
        mockDrone.setState(originState);
        DroneState endState = DroneState.LOADING;
            
        assertThrows(LoadingStateRequirementsNotMetException.class,
        () -> droneStateTransitionService.validateStateTransition(mockDrone, endState));
    }

    @MockBean(LoadingStateRequirementValidator.class)
    LoadingStateRequirementValidator loadingStateRequirementValidatorService() {
        return mock(LoadingStateRequirementValidator.class);
    }
}
