package com.musala.soft.service;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.musala.soft.domain.Drone;
import com.musala.soft.domain.DroneModel;
import com.musala.soft.domain.DroneState;
import com.musala.soft.domain.Medication;
import com.musala.soft.repository.DroneRepository;
import com.musala.soft.repository.MedicationRepository;
import com.musala.soft.repository.DroneModelRepository;
import com.musala.soft.util.dto.in.DroneCreationDto;
import com.musala.soft.util.dto.in.DroneLoadDto;
import com.musala.soft.util.exceptions.DroneLoadExceededException;
import com.musala.soft.util.exceptions.IncorrectDroneStateException;

import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;

@MicronautTest
public class DroneServiceTest {
    
    @Inject
    private DroneService droneService;

    @Inject
    private MedicationRepository medicationRepository;
    
    @Inject
    private DroneRepository droneRepository;
    
    @Inject
    private DroneModelRepository modelRepository;
    
    @Test
    void testDroneCreation() {
        DroneModel model = this.modelRepository.save(new DroneModel("Model name"));
        String serialNumber = "0001";
        Integer weightLimit = 150;
        Integer batteryCapacity = 80;
        DroneCreationDto creationDto = new DroneCreationDto(serialNumber, weightLimit, model.getId(), batteryCapacity);
        
        Drone savedDrone = droneService.createDrone(creationDto);
        
        assertNotNull(savedDrone.getId());
        assertEquals(savedDrone.getBatteryCapacity(), batteryCapacity);
        assertEquals(savedDrone.getWeightLimit(), weightLimit);
        assertEquals(savedDrone.getSerialNumber(), serialNumber);
    }

    @Test
    void testLoadingADroneWithIncorrectStateThrowsIncorrectDroneStateException() {
        DroneState originState = DroneState.IDLE;
        Drone mockDrone = new Drone();
        mockDrone.setWeightLimit(500);
        mockDrone.setState(originState);
        this.droneRepository.save(mockDrone);
        Medication med = this.medicationRepository.save(new Medication("Med_name", 15, "MED_MOCK_1"));
        DroneLoadDto dto = new DroneLoadDto(med.getId(), 5);
            
        assertThrows(IncorrectDroneStateException.class, () -> droneService.loadDrone(mockDrone.getId(), dto));
    }
    
    @Test
    void testExceedingLoadingCapacityThrowsDroneLoadExceededException() {
        DroneState originState = DroneState.LOADING;
        Drone mockDrone = new Drone();
        mockDrone.setWeightLimit(100);
        mockDrone.setState(originState);
        this.droneRepository.save(mockDrone);
        Medication med = this.medicationRepository.save(new Medication("Med_name", 50, "MED_MOCK_1"));
        DroneLoadDto dto = new DroneLoadDto(med.getId(), 20);
            
        assertThrows(DroneLoadExceededException.class, () -> droneService.loadDrone(mockDrone.getId(), dto));
    }

}
