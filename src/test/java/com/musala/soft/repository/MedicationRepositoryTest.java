package com.musala.soft.repository;

import static org.junit.jupiter.api.Assertions.*;

import javax.validation.ConstraintViolationException;

import org.junit.jupiter.api.Test;

import com.musala.soft.domain.Medication;

import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;

@MicronautTest
public class MedicationRepositoryTest {
    
    @Inject
    private MedicationRepository medicationRepository;

    @Test
    void testMedicationWithInvalidNameThrowsException() {
        Medication meds = new Medication("Invalid name 1537", 15, "VALID_CODE85");
        
        assertThrows(ConstraintViolationException.class, () -> this.medicationRepository.save(meds));
    }

    @Test
    void testMedicationWithInvalidCodeThrowsException() {
        Medication meds = new Medication("Valid_name_1537", 15, "INVALID CODE85");
        
        assertThrows(ConstraintViolationException.class, () -> this.medicationRepository.save(meds));
    }
}
