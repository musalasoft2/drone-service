# Musala Soft - Drones API
Version 1.0

## Building and running the app
The application is built to run on docker-compose


Follow the official manual to install docker engine and docker compose:
* [Docker desktop][dockerDesktop]
* [Docker compose][dockerCompose]
 
Once installed open the console at the root directory and run:
```
docker-compose up --build drone
```
This command will download and run a mariaDB image from dockerhub.
It will also run a container with the drone service listening to port 8080 (this can be configured on the docker-compose file)

To test that the application is running open a new tab on:
```
localhost:8080/application
```
You should see a greetings message

## Testing the app
JUnit tests can be ran with the following command on the project's root directory:

```
docker-compose up --build test
```

## Available endpoints

All available endpoints can be viewed and tested with Swagger UI.

With the application running, open a new tab on your web browser and access:

```
localhost:8080/swagger-ui/index.html
```

* To register a new drone send a POST request to:
```
localhost:8080/dispatch/
```
With the body as described on the swagger docs.
Example data:
```
{
    "serialNumber": "A1061206152020",
    "weightLimit": 350,
    "droneModelId": 3
}
```

* To load a drone with medications, first set it to LOADING state by sending the following PUT request:
```
localhost:8080/dispatch/{droneId}/state/loading
```
Once the state has been setm you can start loading by sending POST requests to:
```
localhost:8080/dispatch/{droneId}/load
```
With the body as described on the swagger docs.
Example data:
```
{
    "medicationId" : 24,
    "quantity": 1
}
```

* To check a new's current medication load send a GET request to:
```
localhost:8080/dispatch/{droneId}/load
```

* To check available drones for loading, you must request a list of drones with IDLE status. You can do so by sending a GET request to:
```
localhost:8080/dispatch/state/idle
```

* To check battery level for a given drone send a GET request to:
```
localhost:8080/dispatch/{droneId}/battery
```
* You may also list the medications currently available by sending a GET request to:
```
localhost:8080/medication
```
* As well as a list of drones with a GET request to:
```
localhost:8080/dispatch
```
* And the list of models by sending a GET request to: 
```
localhost:8080/model 
```
* You can also view data for a signle drone with a GET request to
```
localhost:8080/dispatch/{droneId}
```

## Battery level 
Battery levels are logged every hour on a CSV file located on the files directory, at root level.


A new file is created each day to organize the logs by date, and they are formatted in the following way

```
| Timestamp | Serial code | Battery level |
```

> This design assumes that each drone's serial number is unique and won't change over time

[//]: Links

[dockerDesktop]: <https://docs.docker.com/desktop/>
[dockerCompose]: <https://docs.docker.com/compose/install/>
