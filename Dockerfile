FROM openjdk:8u342-jdk
RUN mkdir /app
COPY build.gradle gradle.properties gradlew gradlew.bat micronaut-cli.yml settings.gradle wait-for-it.sh openapi.properties /app/
RUN mkdir /app/files
COPY gradle /app/gradle
WORKDIR /app
RUN chmod +x gradle* wait-for-it.sh
RUN ./gradlew build
COPY src/ /app/src
EXPOSE 8080